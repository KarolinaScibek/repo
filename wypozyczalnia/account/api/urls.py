from django.urls import path
from account.api.views import UserRegisterAPIView

app_name = 'account'

urlpatterns = [
    path('register/', UserRegisterAPIView.as_view(), name ="register")
]