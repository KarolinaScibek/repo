from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from rest_framework.serializers import ModelSerializer, EmailField,CharField, ValidationError

User = get_user_model()


class UserRegisterSerializer(ModelSerializer):
    email = EmailField()
    # w modelu User nie ma atrybutu password2 , musimy sami je stworzyc
    password2 = CharField(style={'input_type': 'password'}, write_only=True, label="Confirm password")

    class Meta:
        model = User
        fields = [
            #'first_name', 'last_name',
            'username', 'email', 'password',
            'password2',
        ]
        # hasło nie będzie widoczne podczas wpisywania go w pole
        extra_kwargs = {'password': {'write_only': True},}

    #sprawdzenie danych podanych przez użytkownika
    def create(self, validated_data):
        #first_name = validated_data['first_name']
        #last_name = validated_data['last_name']
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        password2 = validated_data['password2']

        if password != password2:
            raise ValidationError("Hasła musza być takie same.")
        user_obj = User(username=username, email=email)
        user_obj.set_password(password)
        user_obj.save()
        return validated_data

