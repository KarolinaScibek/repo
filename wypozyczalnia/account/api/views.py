from rest_framework.generics import CreateAPIView
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from account.api.serializers import UserRegisterSerializer

User = get_user_model()


class UserRegisterAPIView(CreateAPIView):
    serializer_class = UserRegisterSerializer
    queryset = User.objects.all()

